create table if not exists public."__EFMigrationsHistory"
(
    "MigrationId"    varchar(150) not null
        constraint "PK___EFMigrationsHistory"
            primary key,
    "ProductVersion" varchar(32)  not null
);

alter table public."__EFMigrationsHistory"
    owner to postgres;

create table if not exists public."Authors"
(
    "Id"              integer generated by default as identity
        constraint "PK_Authors"
            primary key,
    "AuthorGuid"      uuid    not null,
    "Name"            text    not null,
    "Surname"         text    not null,
    "CreatedDate"     timestamp with time zone,
    "CreatedUserGuid" uuid    not null,
    "UpdatedDate"     timestamp with time zone,
    "UpdatedUserGuid" uuid,
    "IsActive"        boolean not null,
    "IsDeleted"       boolean not null
);

alter table public."Authors"
    owner to postgres;

create table if not exists public."Role"
(
    "Id"              integer generated by default as identity
        constraint "PK_Role"
            primary key,
    "RoleGuid"        uuid    not null,
    "Name"            text    not null,
    "Description"     text,
    "CreatedDate"     timestamp with time zone,
    "CreatedUserGuid" uuid    not null,
    "UpdatedDate"     timestamp with time zone,
    "UpdatedUserGuid" uuid,
    "IsActive"        boolean not null,
    "IsDeleted"       boolean not null
);

alter table public."Role"
    owner to postgres;

create table if not exists public."Users"
(
    "Id"              integer generated by default as identity
        constraint "PK_Users"
            primary key,
    "UserGuid"        uuid    not null,
    "UserName"        text    not null,
    "Name"            text    not null,
    "SurName"         text    not null,
    "Password"        text    not null,
    "ProfileImageUrl" text    not null,
    "CreatedDate"     timestamp with time zone,
    "CreatedUserGuid" uuid    not null,
    "UpdatedDate"     timestamp with time zone,
    "UpdatedUserGuid" uuid,
    "IsActive"        boolean not null,
    "IsDeleted"       boolean not null
);

alter table public."Users"
    owner to postgres;

create table if not exists public."Books"
(
    "Id"              integer generated by default as identity
        constraint "PK_Books"
            primary key,
    "BookGuid"        uuid    not null,
    "Name"            text    not null,
    "Description"     text    not null,
    "EanCode"         text    not null,
    "ImageUrl"        text,
    "AuthorGuid"      uuid    not null,
    "AuthorId"        integer not null
        constraint "FK_Books_Authors_AuthorId"
            references public."Authors"
            on delete cascade,
    "CreatedDate"     timestamp with time zone,
    "CreatedUserGuid" uuid    not null,
    "UpdatedDate"     timestamp with time zone,
    "UpdatedUserGuid" uuid,
    "IsActive"        boolean not null,
    "IsDeleted"       boolean not null
);

alter table public."Books"
    owner to postgres;

create index if not exists "IX_Books_AuthorId"
    on public."Books" ("AuthorId");

create table if not exists public."RoleUser"
(
    "RolesId" integer not null
        constraint "FK_RoleUser_Role_RolesId"
            references public."Role"
            on delete cascade,
    "UsersId" integer not null
        constraint "FK_RoleUser_Users_UsersId"
            references public."Users"
            on delete cascade,
    constraint "PK_RoleUser"
        primary key ("RolesId", "UsersId")
);

alter table public."RoleUser"
    owner to postgres;

create index if not exists "IX_RoleUser_UsersId"
    on public."RoleUser" ("UsersId");


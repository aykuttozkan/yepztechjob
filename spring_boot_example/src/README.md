# Spring Boot Example

Proje java 17 ve spring boot 3x versiyonu ile hazırlanmış olup, proje içerisinde hibernate, db olarak postgresql db kullanılmıştır.


### Db Kurulum Adımları

Db olarak postgreql db tercih edilmiş olup, docker üzerinde ilgili db oluşturma işlemi için terminal ekranında aşağıdaki komutu çalıştırınız.

    docker run --name yepz-postgres -e POSTGRES_PASSWORD=123456 -d -p 5430:5432 postgres:alpine

> Proje connection string bilgileri application.properties dosyası içerisinde bulunmaktadır.

> Projede inmemory login özelliği bulunmakta olup, bilgiler _**com.yepz.demo.config.security**_ paketi altında ki __**WebSecurityConfig**__ sınıfı içerisinde tanımlanmıştır.

harici olarak proje ana sayfasında kullanıcı bilgileri bulunmaktadır.


### Örnek api bilgileri;

* /api/kitap tüm kitap listesini getirir
* /api/kitap/1 sadece 1 nolu id bilgisine ait kitap bilgisini getirir
* /api/kitap/ekle otomatik random bir kitap eklemek için oluşturulmuştur.

> kitap endpoint'i ADMIN rolüne sahip kullanıcılar tarafından erişilebilecek şekilde ayarlanmıştır.

> Proje genel hatları ile bir demo olup, normak insert update delete operasyonları için restfull yapısı dikkate alınmamıştır. Bu nedenden sadece random insert işlemi için bir adet endpoint bırakılmış, başka özellik eklenmemiştir.

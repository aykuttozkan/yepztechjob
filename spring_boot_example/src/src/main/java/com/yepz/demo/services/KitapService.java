package com.yepz.demo.services;

import com.yepz.demo.models.Kitap;
import com.yepz.demo.repositories.KitapRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
@AllArgsConstructor
public class KitapService {
    final KitapRepository _kitapRepository;

    public ResponseEntity<List<Kitap>> GetAll() throws Exception {
        List<Kitap> result = _kitapRepository.findAll().stream().toList();

        if (result.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    public ResponseEntity<Kitap> getKitapById(long id) throws Exception {
        Kitap result = _kitapRepository.getKitapById(id);

        if (null == result)
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    public ResponseEntity<Kitap> otomatikKitapOlustur() throws Exception {
        Random random = new Random();
        Kitap kitap = new Kitap();

        String randomId = String.valueOf(random.nextInt(0, 5000));

        kitap.setAdi("Kitap".concat(" ").concat(randomId));
        kitap.setIcerik("Kitap".concat(" ").concat(randomId).concat(" İçerik"));

        _kitapRepository.save(kitap);

        return new ResponseEntity<>(kitap, HttpStatus.OK);
    }
}

package com.yepz.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
    @GetMapping("/")
    @ResponseBody
    public String Index(){
        StringBuilder sb = new StringBuilder();

        sb.append("Merhabalar.Örnek verileri görmek için ");
        sb.append("<a href='/api/kitap'>Tıklayınız</a>");
        sb.append("<br/> NOT! bu linke erişebilemek admin rolüne sahip olmanız gerekmektedir.<br>");
        sb.append("Admin kullanıcısı admin:admin<br>");
        sb.append("Demo kullanıcısı demo:demo");

        return sb.toString();
    }
}

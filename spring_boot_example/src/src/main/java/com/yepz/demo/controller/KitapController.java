package com.yepz.demo.controller;

import com.yepz.demo.models.Kitap;
import com.yepz.demo.services.KitapService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/kitap")
@AllArgsConstructor
public class KitapController {
    //final KitapRepository _kitapRepository;
    final KitapService _kitapService;

    @GetMapping(value = "", produces = "application/json")
    public ResponseEntity<List<Kitap>> GetAllIndex() {
        return GetAll();
    }

    @GetMapping(value = "/", produces = "application/json")
    public ResponseEntity<List<Kitap>> GetAll() {
        try {
            return _kitapService.GetAll();
        } catch (Exception e) {
            System.out.println(e.getMessage());

            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Kitap> Get(@PathVariable long id) {
        try {
            return _kitapService.getKitapById(id);
        } catch (Exception e) {
            System.out.println(e.getMessage());

            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Bu method her tetiklendiginde yeni bir kitap icerigi olusturur.
     * @return
     */
    @GetMapping(value = "/ekle", produces = "application/json")
    public ResponseEntity<Kitap> OtomatikKitapOlusturFromGetMethod() {
        try {
            return _kitapService.otomatikKitapOlustur();
        } catch (Exception e) {
            System.out.println(e.getMessage());

            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

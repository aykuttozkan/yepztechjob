package com.yepz.demo.repositories;

import com.yepz.demo.models.Kitap;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface KitapRepository extends JpaRepository<Kitap, Long> {
    List<Kitap> getAllByAdi(String adi);
    Kitap getKitapById(long id);
}

package com.yepz.demo.models;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "kitaplar")
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Kitap {
    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Getter
    @Setter
    @Column(name = "adi")
    private String adi;

    @Getter
    @Setter
    @Column(name = "icerik")
    private String icerik;
}

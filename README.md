## Yepz Tech Job Dokümantasyonu

Proje katmanlı mimari (clean architecture) olarak hazırlamıştır.

Read ve write operasyonları cqrs ile ayrıldığı gibi repository operasyonları da ayrılmıştır.

Db olarak _**PostgreSQL**_ tercih edilmiş olup, _**Code First**_ yaklaşımı benimsenmiştir.

Dokümantasyon olarak api projesi için _**Swagger**_ arayüzü mevcuttur.

Proje katmanları ve açıklaması.

1. **Presentation**
    
    Proje api katmanını barındırmaktadır. _**Program.cs**_ içerisinde katmanlara ait _**ServiceRegistration.cs**_ methodları çağrılarak initialize işlemleri ilgili katmanlar altında yapılmıştır.

2. **Domain**
    
    Entity model nesnelerinin bulunduğu katmandır.

    * Db entity modelleri **Entity** klasörü altında bulunmaktadır.
    * Tüm entity modelleri **BaseEntity** sınıfından türemektedir.

3. **Application**

    * Bu katman bussines operasyonlarının yürütüldüğü katmandır.
    İçerisinde _**Mediator Pattern**_ kullanılmış olup, oto mapping işlemleri için _**AutoMapper**_ kullanılmıştır.

    * Map işlemleri **Mapping** klasörü altındaki sınıfların içerinde tanımlanmıştır.

    * CQRS işlemleri ilgili operasyonlar için bölümlendirilerek hepsi **CQRS** altında toplanmıştır.

    * Proje üzerinde kullanılcak yapılar **Interface** klasörü altında tanımlanmıştır.

    * Dependency Injection tanımlamaları **ServiceRegistration** klasörü altındaki sınıf üzerinden yapılmaktadır.

4. **Persistance**

    Bu katmanda üzerinde _**Migration**_ klasörü ve db operasyonlarını için gerekli olan _**DbContext**_ ve _**Repository**_ nesneleri barındırılmaktadır.

    * **Context** klasörü altında dbcontext nesnesi bulunmaktadır.
    
    * **Extension** klasörü altında **Seed** operasyonu için gerekli olan genişletme sınıfı bulunmaktadır.
    
    * **Migrations** klasörü altında db migration işlemleri için gerekli olan ilgili komutlar bulunmaktadır.

    * **Repositories** klasörü altında hem generic **Read ve Write** repository sınıfları hemde ilgili entity nesnelerine ait **Read ve Write** repository sınıfları bulunmaktadır.

    * Dependency Injection tanımlamaları **ServiceRegistration** klasörü altındaki sınıf üzerinden yapılmaktadır.

5. **Commons**

    Bu katman tüm katmanların ortak olarak kullanabileceği genel helper, extension vb. operasyonlar için gerekli olan sınıf ve kütüphaneleri barındırmaktadır.

6. **UnitTest**

    Bu katman projede için gerekli olacak Test operasyonlarına ait test sınıflarını barındırmaktadır.

### Dotnet Ef Tools Kurulumu

Öncelikle cli üzerinden ef core migration komutlarını çalıştırabilmek için aşağıdaki komutu komut satırında çalıştırınız.

    dotnet tool install --global dotnet-ef

eğer sistemde yüklü bir dotnet-ef tool var ise güncellemek için aşağıdaki komutu kullanınız.

    dotnet tool update --global dotnet-ef


### Dotnet ef tools ile Code First tablo oluşturma komutu;

Aşağıdaki komutu ana dizin altında cmd programı açılarak çalıştırılır.

Aşağıdaki komut ile migration dosyası oluşturulur.

    dotnet ef migrations add "Initialize(Migration Adi)" --context ApplicationDbContext --project Infrastructure/Yepz.TechJob.Persistance/Yepz.TechJob.Persistance.csproj --startup-project Presentation/Yepz.TechJob.Api/Yepz.TechJob.Api.csproj -o Migrations


Oluşturulan migration dosyalarının db üzerine yansıtılması için aşağıdaki komut çalıştırılır.

    dotnet ef database update --context ApplicationDbContext --project Infrastructure/Yepz.TechJob.Persistance/Yepz.TechJob.Persistance.csproj --startup-project Presentation/Yepz.TechJob.Api/Yepz.TechJob.Api.csproj  -- --environment Development


Migration listesi için aşağıdaki komutu çalıştırabilirsiniz.

    dotnet ef migrations list --context ApplicationDbContext --project Infrastructure/Yepz.TechJob.Persistance/Yepz.TechJob.Persistance.csproj  --startup-project Presentation/Yepz.TechJob.Api/Yepz.TechJob.Api.csproj

### Örnek Kullanıcı Bilgileri

* admin : 123123
* demo : 123123
* demo2 : 123123

### Db Kurulum Adımları

Db olarak postgreql db tercih edilmiş olup, docker üzerinde ilgili db oluşturma işlemi için terminal ekranında aşağıdaki komutu çalıştırınız.

    docker run --name techjobpgsql -e POSTGRES_PASSWORD=mysecretpassword -p 5435:5432 -d postgres:alpine

> Code first yapısını kullanmadan sadece ddl scriptler ile tablo oluşturulmak istenirse [DDL Script](sql_ddl_script.sql) dosyasını indirip ilgili db üzerinde çalıştırabilirsiniz.

### Ekran Görüntüleri

* ![Db Tablo Yapısı](screenshots/db_tablo_yapisi_02.png)


* Login İşlemi 
    ![Login](screenshots/login_01.png)

* Kitap Ekleme
    ![Kitap Ekleme](screenshots/kitap_ekleme_02.png)
    ![Kitap Ekleme](screenshots/kitap_ekleme_03.png)

### Kaynaklar

* [Entity Framework Core Documentation](https://learn.microsoft.com/en-us/ef/core/cli/dotnet)

﻿using AutoMapper;
using Yepz.TechJob.Application.CQRS.BookCQRS.Commands.Request;
using Yepz.TechJob.Application.CQRS.BookCQRS.Commands.Response;
using Yepz.TechJob.Application.CQRS.BookCQRS.Queries.Response;
using Yepz.TechJob.Domain.Entities;

namespace Yepz.TechJob.Application.Mapping;

public class BookMapping : Profile
{
    public BookMapping()
    {
        CreateMap<CreateBookCommandRequest, Book>()
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
            .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
            .ForMember(dest => dest.EanCode, opt => opt.MapFrom(src => src.EanCode))
            .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.ImageUrl));

        CreateMap<CreateBookCommandResponse, Book>()
            .ForPath(dest => dest.Author.Name, opt => opt.MapFrom(src => src.AuthorName))
            .ForPath(dest => dest.Author.Surname, opt => opt.MapFrom(src => src.AuthorSurname))
            .ReverseMap();

        CreateMap<AllListBookQueryResponse, Book>()
            .ForPath(dest => dest.Author.Name, opt => opt.MapFrom(src => src.AuthorName))
            .ForPath(dest => dest.Author.Surname, opt => opt.MapFrom(src => src.AuthorSurname))
            .ReverseMap();
    }
}


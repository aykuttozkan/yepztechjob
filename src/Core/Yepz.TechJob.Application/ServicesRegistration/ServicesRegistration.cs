﻿using Microsoft.Extensions.DependencyInjection;

namespace Yepz.TechJob.Application.ServicesRegistration;

public static class ServicesRegistration
{
	public static IServiceCollection AddApplicationServiceRegistration(this IServiceCollection serviceCollection)
	{
        // proje asseblies bilgilerini dinamik olarak aliyoruz
        var assemblies = AppDomain.CurrentDomain.GetAssemblies();

        // http-s://dev.to/moe23/add-automapper-to-net-6-3fdn
        serviceCollection.AddAutoMapper(assemblies);

        // http-s://github.com/jbogard/MediatR
        serviceCollection.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(assemblies));

        return serviceCollection;
	}
}

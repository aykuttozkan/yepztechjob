﻿using Yepz.TechJob.Domain.Entities;

namespace Yepz.TechJob.Application.Interfaces;

public interface IUserReadRepository : IReadRepository<User>
{

}


﻿using Yepz.TechJob.Domain.Entities;

namespace Yepz.TechJob.Application.Interfaces;

public interface IBookWriteRepository : IWriteRepository<Book>
{
}


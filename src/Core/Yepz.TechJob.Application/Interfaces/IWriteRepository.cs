﻿using Yepz.TechJob.Domain.Common;

namespace Yepz.TechJob.Application.Interfaces;

public interface IWriteRepository<T> where T : BaseEntity
{
    Task<T> AddAsync(T entity);

    void Delete(T entity);

    Task DeleteByIdAsync(Guid id);

    Task<int> SaveChanges();

    Task<int> SaveChanges(CancellationToken cancellationToken);
}


﻿using System;
using Yepz.TechJob.Domain.Common;
using Yepz.TechJob.Domain.Entities;

namespace Yepz.TechJob.Application.Interfaces;

public interface IBookReadRepository : IReadRepository<Book>
{

}

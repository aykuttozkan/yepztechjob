﻿using System.Linq.Expressions;
using Yepz.TechJob.Domain.Common;

namespace Yepz.TechJob.Application.Interfaces;

public interface IReadRepository<T> where T : BaseEntity
{
    Task<T> GetByIdAsync(Guid id);

    Task<T> GetByIdAsync(Guid id, bool isAsNoTracking);

    IQueryable<T> GetAllQueryable();

    IQueryable<T> GetAllQueryable(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includes);
}

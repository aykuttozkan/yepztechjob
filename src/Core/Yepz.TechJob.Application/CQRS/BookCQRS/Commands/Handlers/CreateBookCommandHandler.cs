﻿using AutoMapper;
using MediatR;
using Yepz.TechJob.Application.CQRS.BookCQRS.Commands.Request;
using Yepz.TechJob.Application.CQRS.BookCQRS.Commands.Response;
using Yepz.TechJob.Application.Interfaces;
using Yepz.TechJob.Commons.Models;
using Yepz.TechJob.Domain.Entities;

namespace Yepz.TechJob.Application.CQRS.BookCQRS.Commands.Handlers;

public class CreateBookCommand : IRequest<ResultModel<CreateBookCommandResponse>>
{
    private readonly CreateBookCommandRequest _createBookCommandRequest;

    public CreateBookCommand(CreateBookCommandRequest createBookCommandRequest)
    {
        _createBookCommandRequest = createBookCommandRequest;
    }

    private class CreateBookCommandHandler : IRequestHandler<CreateBookCommand, ResultModel<CreateBookCommandResponse>>
    {
        private readonly IMapper _mapper;
        private readonly IBookWriteRepository _bookWriteRepository;
        private readonly IAuthorReadRepository _authorReadRepository;

        public CreateBookCommandHandler(IMapper mapper, IBookWriteRepository bookWriteRepository, IAuthorReadRepository authorReadRepository)
        {
            _mapper = mapper;
            _bookWriteRepository = bookWriteRepository;
            _authorReadRepository = authorReadRepository;
        }

        public async Task<ResultModel<CreateBookCommandResponse>> Handle(CreateBookCommand request, CancellationToken cancellationToken)
        {
            ResultModel<CreateBookCommandResponse> result = new();

            if (request is null)
            {
                result.ErrorMessage = string.Format("{0} degeri null olamaz", nameof(CreateBookCommand));

                return await Task.FromResult(result);
            }

            if (request._createBookCommandRequest.AuthorGuid == default)
            {
                result.ErrorMessage = string.Format("{0} degeri null olamaz", nameof(request._createBookCommandRequest.AuthorGuid));

                return await Task.FromResult(result);
            }

            var author = _authorReadRepository.GetAllQueryable(x => x.AuthorGuid.Equals(request._createBookCommandRequest.AuthorGuid)).FirstOrDefault();

            if (author is null)
            {
                result.ErrorMessage = string.Format("Gecersiz veya hatalı yazar {0} bilgisi", nameof(request._createBookCommandRequest.AuthorGuid));

                return await Task.FromResult(result);
            }

            var book = _mapper.Map<Book>(request._createBookCommandRequest);

            book.BookGuid = Guid.NewGuid();
            book.Author = author;
            book.AuthorGuid = author.AuthorGuid;

            await _bookWriteRepository.AddAsync(book);

            await _bookWriteRepository.SaveChanges(cancellationToken);


            result.Result = _mapper.Map<CreateBookCommandResponse>(book);

            return await Task.FromResult(result);
        }
    }
}

﻿using Yepz.TechJob.Domain.Entities;

namespace Yepz.TechJob.Application.CQRS.BookCQRS.Commands.Response;

public class CreateBookCommandResponse
{
    public Guid BookGuid { get; set; }

    public required string Name { get; set; }

    public string Description { get; set; } = string.Empty;

    public required string EanCode { get; set; }

    public string? ImageUrl { get; set; }

    public Guid AuthorGuid { get; set; } // Required foreign key property

    //public Author Author { get; set; } = default!;

    public string AuthorName { get; set; } = string.Empty;

    public string AuthorSurname { get; set; } = string.Empty;
}
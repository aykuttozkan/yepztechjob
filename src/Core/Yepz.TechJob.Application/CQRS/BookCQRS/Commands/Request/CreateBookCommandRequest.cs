﻿namespace Yepz.TechJob.Application.CQRS.BookCQRS.Commands.Request;

public class CreateBookCommandRequest
{
    public required string Name { get; set; }

    public string? Description { get; set; }

    public required string EanCode { get; set; }

    public string? ImageUrl { get; set; }

    public required Guid AuthorGuid { get; set; }
}

﻿using Yepz.TechJob.Domain.Entities;

namespace Yepz.TechJob.Application.CQRS.BookCQRS.Queries.Response;

public class AllListBookQueryResponse
{
    public AllListBookQueryResponse()
    {

    }

    public int Id { get; set; }

    public Guid BookGuid { get; set; }

    public required string Name { get; set; }

    public string Description { get; set; } = string.Empty;

    public required string EanCode { get; set; }

    public string? ImageUrl { get; set; }

    public Guid AuthorGuid { get; set; } // Required foreign key property

    public string AuthorName { get; set; } = string.Empty;

    public string AuthorSurname { get; set; } = string.Empty;

    public DateTime? CreatedDate { get; set; }

    public Guid CreatedUserGuid { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public Guid? UpdatedUserGuid { get; set; }

    public bool IsActive { get; set; } = true;

    public bool IsDeleted { get; set; } = false;
}


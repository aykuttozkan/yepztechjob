﻿using AutoMapper;
using MediatR;
using Yepz.TechJob.Application.CQRS.BookCQRS.Queries.Response;
using Yepz.TechJob.Application.Interfaces;
using Yepz.TechJob.Commons.Models;
using Yepz.TechJob.Domain.Entities;

namespace Yepz.TechJob.Application.CQRS.BookCQRS.Queries.Handlers;

public class AllListBookQuery : IRequest<ResultModel<List<AllListBookQueryResponse>>>
{

    private class AllListBookQueryHandler : IRequestHandler<AllListBookQuery, ResultModel<List<AllListBookQueryResponse>>>
    {
        private readonly IMapper _mapper;
        private readonly IBookReadRepository _bookReadRepository;

        public AllListBookQueryHandler(IMapper mapper, IBookReadRepository bookReadRepository)
        {
            _mapper = mapper;
            _bookReadRepository = bookReadRepository;
        }

        public async Task<ResultModel<List<AllListBookQueryResponse>>> Handle(AllListBookQuery request, CancellationToken cancellationToken)
        {
            ResultModel<List<AllListBookQueryResponse>> result = new();

            List<Book> bookList = _bookReadRepository.GetAllQueryable(x => !x.IsDeleted, i1 => i1.Author).ToList();

            result.Result = _mapper.Map<List<AllListBookQueryResponse>>(bookList);

            return await Task.FromResult(result);
        }
    }
}

﻿namespace Yepz.TechJob.Application.CQRS.UserCQRS.Queries.Request;

public class GetUserByNameAndPasswordQueryRequest
{
    public string UserName { get; set; } = string.Empty;

    public string Password { get; set; } = string.Empty;
}


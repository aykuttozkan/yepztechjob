﻿using MediatR;
using Yepz.TechJob.Application.CQRS.UserCQRS.Queries.Request;
using Yepz.TechJob.Application.Interfaces;
using Yepz.TechJob.Commons.Extensions;
using Yepz.TechJob.Commons.Models;
using Yepz.TechJob.Domain.Entities;

namespace Yepz.TechJob.Application.CQRS.UserCQRS.Queries.Handlers;

public class GetUserByUserNameAndPasswordQuery : IRequest<ResultModel<User>>
{
    protected readonly GetUserByNameAndPasswordQueryRequest _model;

    public GetUserByUserNameAndPasswordQuery(GetUserByNameAndPasswordQueryRequest model)
    {
        _model = model;
    }


    private class GetUserByUserNameAndPasswordQueryHandler : IRequestHandler<GetUserByUserNameAndPasswordQuery, ResultModel<User>>
    {
        private readonly IUserReadRepository _userReadRepository;

        public GetUserByUserNameAndPasswordQueryHandler(IUserReadRepository userReadRepository)
        {
            _userReadRepository = userReadRepository;
        }

        public async Task<ResultModel<User>> Handle(GetUserByUserNameAndPasswordQuery request, CancellationToken cancellationToken)
        {
            ResultModel<User> result = new();

            if(string.IsNullOrWhiteSpace(request._model.UserName) || string.IsNullOrWhiteSpace(request._model.Password))
            {
                result.ErrorMessage = "Lütfen girmiş olduğunuz bilgileri kontrol ediniz";

                return await Task.FromResult(result);
            }

            var userResult = _userReadRepository
                .GetAllQueryable(
                    x => x.UserName.Equals(request._model.UserName)
                    && x.Password.Equals(request._model.Password.ComputeHashInLowerCase(HashingClassAlgorithms.MD5).ComputeHashInLowerCase(HashingClassAlgorithms.SHA256))
                    && !x.IsDeleted, i1 => i1.Roles
                ).FirstOrDefault();

            result.Result = userResult ?? new();

            return await Task.FromResult(result);
        }
    }
}
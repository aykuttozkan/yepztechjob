﻿using System.ComponentModel.DataAnnotations.Schema;
using Yepz.TechJob.Domain.Common;

namespace Yepz.TechJob.Domain.Entities;

public class Book : BaseEntity
{
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid BookGuid { get; set; }

    public required string Name { get; set; }

    public string Description { get; set; } = string.Empty;

    public required string EanCode { get; set; }

    public string? ImageUrl { get; set; }

    public Guid AuthorGuid { get; set; } // Required foreign key property

    public Author Author { get; set; } = default!;
}

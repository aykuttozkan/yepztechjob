﻿using System.ComponentModel.DataAnnotations.Schema;
using Yepz.TechJob.Domain.Common;

namespace Yepz.TechJob.Domain.Entities;

public class User : BaseEntity
{
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid UserGuid { get; set; }

    public string UserName { get; set; } = string.Empty;

    public string Name { get; set; } = string.Empty;

    public string SurName { get; set; } = string.Empty;

    public string Password { get; set; } = string.Empty;

    public string ProfileImageUrl { get; set; } = string.Empty;

    public virtual ICollection<Role> Roles { get; set; } = new List<Role>();
}

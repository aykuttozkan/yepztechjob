﻿using System.ComponentModel.DataAnnotations.Schema;
using Yepz.TechJob.Domain.Common;

namespace Yepz.TechJob.Domain.Entities;

public class Author : BaseEntity
{
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid AuthorGuid { get; set; }

	public required string Name { get; set; }

	public required string Surname { get; set; }

	public ICollection<Book> Books { get; set; } = new List<Book>();
}

﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Yepz.TechJob.Domain.Common;

namespace Yepz.TechJob.Domain.Entities
{
	public class Role : BaseEntity
	{
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid RoleGuid { get; set; }

		public required string Name { get; set; }

		public string? Description { get; set; } = string.Empty;

		public virtual ICollection<User> Users { get; set; } = new List<User>();
	}
}

﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Yepz.TechJob.Application.CQRS.UserCQRS.Queries.Handlers;
using Yepz.TechJob.Application.CQRS.UserCQRS.Queries.Request;
using Yepz.TechJob.Commons.Models;
using Yepz.TechJob.Domain.Entities;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Yepz.TechJob.Api.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        /*// GET: api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }*/

        private readonly IConfiguration _configuration;
        private readonly IMediator _mediator;

        public AccountController(IConfiguration configuration, IMediator mediator)
        {
            _configuration = configuration;
            _mediator = mediator;
        }

        // POST api/values
        [HttpPost]
        public async Task<ResultModel<string>> Post([FromBody] GetUserByNameAndPasswordQueryRequest model)
        {
            /*var userRoles = await userManager.GetRolesAsync(user);
            var authClaims = new List<Claim>
            {
               new Claim(ClaimTypes.Name, user.UserName),
               new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            foreach (var userRole in userRoles)
            {
                authClaims.Add(new Claim(ClaimTypes.Role, userRole));
            }
            string token = GenerateToken(authClaims);*/
            ResultModel<string> result = new();

            var userResult = await _mediator.Send(new GetUserByUserNameAndPasswordQuery(model));


            if (userResult.Result == null || userResult.Result.UserName is null)
            {
                result.ErrorMessage = "Hatalı kullanıcı adı veya şifre";

                return await Task.FromResult(result);
            }

            var authClaims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, userResult.Result.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, userResult.Result.UserGuid.ToString())
            };

            foreach (var role in userResult.Result.Roles)
            {
                authClaims.Add(new Claim(ClaimTypes.Role, role.Name));
            }

            result.Result = GenerateToken(authClaims);

            return await Task.FromResult(result);
        }


        private string GenerateToken(IEnumerable<Claim> claims)
        {
            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWTKey:Secret"]));
            var _TokenExpiryTimeInHour = Convert.ToInt64(_configuration["JWTKey:TokenExpiryTimeInHour"]);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = _configuration["JWTKey:ValidIssuer"],
                Audience = _configuration["JWTKey:ValidAudience"],
                //Expires = DateTime.UtcNow.AddHours(_TokenExpiryTimeInHour),
                Expires = DateTime.UtcNow.AddMinutes(30),
                SigningCredentials = new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256),
                Subject = new ClaimsIdentity(claims)
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}


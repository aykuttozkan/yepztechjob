﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Yepz.TechJob.Application.CQRS.BookCQRS.Commands.Handlers;
using Yepz.TechJob.Application.CQRS.BookCQRS.Commands.Request;
using Yepz.TechJob.Application.CQRS.BookCQRS.Commands.Response;
using Yepz.TechJob.Application.CQRS.BookCQRS.Queries.Handlers;
using Yepz.TechJob.Application.CQRS.BookCQRS.Queries.Response;
using Yepz.TechJob.Application.Interfaces;
using Yepz.TechJob.Commons.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Yepz.TechJob.Api.Controllers;

[Route("api/[controller]")]
public class BooksController : ControllerBase
{
    private readonly IMediator _mediator;

    public BooksController(IMediator mediator, IBookReadRepository bookReadRepository)
    {
        _mediator = mediator;
    }

    [HttpGet]
    [Authorize(Roles = "Admin,User")]
    public async Task<ResultModel<List<AllListBookQueryResponse>>> Get()
    {
        return await _mediator.Send(new AllListBookQuery());
    }

    [HttpPost]
    [Authorize(Roles = "Admin")]
    public async Task<ResultModel<CreateBookCommandResponse>> Post([FromBody] CreateBookCommandRequest model)
    {
        return await _mediator.Send(new CreateBookCommand(model));
    }
}

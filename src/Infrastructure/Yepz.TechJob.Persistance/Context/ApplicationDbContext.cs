﻿using Microsoft.EntityFrameworkCore;
using Yepz.TechJob.Domain.Entities;
using Yepz.TechJob.Persistance.Extensions;

namespace Yepz.TechJob.Persistance.Context;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }

    public DbSet<Author> Authors { get; set; }

    public DbSet<Book> Books { get; set; }

    public DbSet<User> Users { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Seed data
        modelBuilder.Seed();

        base.OnModelCreating(modelBuilder);
    }
}

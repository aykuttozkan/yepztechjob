﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Yepz.TechJob.Application.Interfaces;
using Yepz.TechJob.Persistance.Context;
using Yepz.TechJob.Persistance.Repositories;

namespace Yepz.TechJob.Persistance.ServiceRegistration;

public static class ServiceRegistration
{
    public static IServiceCollection AddPersistanceServiceRegistration(this IServiceCollection serviceProviders, ConfigurationManager configurationManager)
    {
        serviceProviders.AddDbContext<ApplicationDbContext>(opt =>
        {
            opt.UseNpgsql(configurationManager.GetConnectionString("TechJobDbConnection"));
            opt.EnableSensitiveDataLogging();
        });

        serviceProviders.AddScoped<DbContext, ApplicationDbContext>();
        // repository dependency injection definition
        serviceProviders.AddScoped<IBookWriteRepository, BookWriteRepository>();
        serviceProviders.AddScoped<IBookReadRepository, BookReadRepository>();

        serviceProviders.AddScoped<IAuthorReadRepository, AuthorReadRepository>();

        serviceProviders.AddScoped<IUserReadRepository, UserReadRepository>();

        return serviceProviders;
    }
}


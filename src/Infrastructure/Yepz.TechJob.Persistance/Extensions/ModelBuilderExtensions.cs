﻿using Microsoft.EntityFrameworkCore;
using Yepz.TechJob.Commons.Extensions;
using Yepz.TechJob.Domain.Entities;

namespace Yepz.TechJob.Persistance.Extensions;

public static class ModelBuilderExtensions
{
    public static void Seed(this ModelBuilder modelBuilder)
    {
        var firstRole = new Role { Id = 3, RoleGuid = Guid.NewGuid(), Name = "Admin", Description = "full yetkili kullanici" };
        var secondRole = new Role { Id = 5, RoleGuid = Guid.NewGuid(), Name = "User", Description = "user yetkili kullanici" };

        modelBuilder.Entity<Role>().HasMany(p => p.Users).WithMany(p => p.Roles);

        modelBuilder.Entity<Role>().HasData(firstRole, secondRole);

        var firstUser = new User {
            Id = 4,
            UserName = "admin",
            Password = "123123".ComputeHashInLowerCase(HashingClassAlgorithms.MD5).ComputeHashInLowerCase(HashingClassAlgorithms.SHA256),
            UserGuid = Guid.NewGuid(),
            Roles = new List<Role>()
        };

        var secondUser = new User { Id = 5, UserName = "demo", Password = "123123".ComputeHashInLowerCase(HashingClassAlgorithms.MD5).ComputeHashInLowerCase(HashingClassAlgorithms.SHA256), UserGuid = Guid.NewGuid() };
        var thirdUser = new User { Id = 6, UserName = "demo2", Password = "123123".ComputeHashInLowerCase(HashingClassAlgorithms.MD5).ComputeHashInLowerCase(HashingClassAlgorithms.SHA256), UserGuid = Guid.NewGuid() };

        modelBuilder.Entity<User>().HasMany(p => p.Roles).WithMany(p => p.Users);

        modelBuilder.Entity<User>().HasData(firstUser, secondUser, thirdUser);

        Guid firstAuthorGuid = Guid.NewGuid();
        Guid secondAuthorGuid = Guid.NewGuid();

        modelBuilder.Entity<Author>().HasData(
            new Author { Id = 1, Name = "Yazar 1", Surname = "Soyadı 1", AuthorGuid = firstAuthorGuid, CreatedUserGuid = firstUser.UserGuid },
            new Author { Id = 2, Name = "Yazar 2", Surname = "Soyadı 2", AuthorGuid = secondAuthorGuid, CreatedUserGuid = secondUser.UserGuid }
        );
    }
}

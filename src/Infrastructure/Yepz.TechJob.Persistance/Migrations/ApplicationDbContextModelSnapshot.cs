﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using Yepz.TechJob.Persistance.Context;

#nullable disable

namespace Yepz.TechJob.Persistance.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    partial class ApplicationDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.13")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            NpgsqlModelBuilderExtensions.UseIdentityByDefaultColumns(modelBuilder);

            modelBuilder.Entity("RoleUser", b =>
                {
                    b.Property<int>("RolesId")
                        .HasColumnType("integer");

                    b.Property<int>("UsersId")
                        .HasColumnType("integer");

                    b.HasKey("RolesId", "UsersId");

                    b.HasIndex("UsersId");

                    b.ToTable("RoleUser");
                });

            modelBuilder.Entity("Yepz.TechJob.Domain.Entities.Author", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<int>("Id"));

                    b.Property<Guid>("AuthorGuid")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<DateTime?>("CreatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("timestamp with time zone");

                    b.Property<Guid>("CreatedUserGuid")
                        .HasColumnType("uuid");

                    b.Property<bool>("IsActive")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("Surname")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<DateTime?>("UpdatedDate")
                        .HasColumnType("timestamp with time zone");

                    b.Property<Guid?>("UpdatedUserGuid")
                        .HasColumnType("uuid");

                    b.HasKey("Id");

                    b.ToTable("Authors");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            AuthorGuid = new Guid("e5d3eedd-a1c3-46c5-a9ce-19370197c9be"),
                            CreatedUserGuid = new Guid("9dd43fcc-3810-4935-b22a-42115edc16cc"),
                            IsActive = true,
                            IsDeleted = false,
                            Name = "Yazar 1",
                            Surname = "Soyadı 1"
                        },
                        new
                        {
                            Id = 2,
                            AuthorGuid = new Guid("6200de17-bcc5-408c-9133-8f61c20f40bd"),
                            CreatedUserGuid = new Guid("83b50aa5-1ca5-4f2d-9a69-c0d314c5f7f7"),
                            IsActive = true,
                            IsDeleted = false,
                            Name = "Yazar 2",
                            Surname = "Soyadı 2"
                        });
                });

            modelBuilder.Entity("Yepz.TechJob.Domain.Entities.Book", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<int>("Id"));

                    b.Property<Guid>("AuthorGuid")
                        .HasColumnType("uuid");

                    b.Property<int>("AuthorId")
                        .HasColumnType("integer");

                    b.Property<Guid>("BookGuid")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<DateTime?>("CreatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("timestamp with time zone");

                    b.Property<Guid>("CreatedUserGuid")
                        .HasColumnType("uuid");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("EanCode")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("ImageUrl")
                        .HasColumnType("text");

                    b.Property<bool>("IsActive")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<DateTime?>("UpdatedDate")
                        .HasColumnType("timestamp with time zone");

                    b.Property<Guid?>("UpdatedUserGuid")
                        .HasColumnType("uuid");

                    b.HasKey("Id");

                    b.HasIndex("AuthorId");

                    b.ToTable("Books");
                });

            modelBuilder.Entity("Yepz.TechJob.Domain.Entities.Role", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<int>("Id"));

                    b.Property<DateTime?>("CreatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("timestamp with time zone");

                    b.Property<Guid>("CreatedUserGuid")
                        .HasColumnType("uuid");

                    b.Property<string>("Description")
                        .HasColumnType("text");

                    b.Property<bool>("IsActive")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<Guid>("RoleGuid")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<DateTime?>("UpdatedDate")
                        .HasColumnType("timestamp with time zone");

                    b.Property<Guid?>("UpdatedUserGuid")
                        .HasColumnType("uuid");

                    b.HasKey("Id");

                    b.ToTable("Role");

                    b.HasData(
                        new
                        {
                            Id = 3,
                            CreatedUserGuid = new Guid("00000000-0000-0000-0000-000000000000"),
                            Description = "full yetkili kullanici",
                            IsActive = true,
                            IsDeleted = false,
                            Name = "Admin",
                            RoleGuid = new Guid("240340eb-dadb-470b-93f8-39d8755940c3")
                        },
                        new
                        {
                            Id = 5,
                            CreatedUserGuid = new Guid("00000000-0000-0000-0000-000000000000"),
                            Description = "user yetkili kullanici",
                            IsActive = true,
                            IsDeleted = false,
                            Name = "User",
                            RoleGuid = new Guid("00d371f5-81cc-4c38-b949-bfc324c90327")
                        });
                });

            modelBuilder.Entity("Yepz.TechJob.Domain.Entities.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<int>("Id"));

                    b.Property<DateTime?>("CreatedDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("timestamp with time zone");

                    b.Property<Guid>("CreatedUserGuid")
                        .HasColumnType("uuid");

                    b.Property<bool>("IsActive")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("ProfileImageUrl")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("SurName")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<DateTime?>("UpdatedDate")
                        .HasColumnType("timestamp with time zone");

                    b.Property<Guid?>("UpdatedUserGuid")
                        .HasColumnType("uuid");

                    b.Property<Guid>("UserGuid")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<string>("UserName")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("Users");

                    b.HasData(
                        new
                        {
                            Id = 4,
                            CreatedUserGuid = new Guid("00000000-0000-0000-0000-000000000000"),
                            IsActive = true,
                            IsDeleted = false,
                            Name = "",
                            Password = "04f96727bb95e8cd75455822a7472e99a3fa14ce8098ffc5ce4a73ef07dde3fe",
                            ProfileImageUrl = "",
                            SurName = "",
                            UserGuid = new Guid("9dd43fcc-3810-4935-b22a-42115edc16cc"),
                            UserName = "admin"
                        },
                        new
                        {
                            Id = 5,
                            CreatedUserGuid = new Guid("00000000-0000-0000-0000-000000000000"),
                            IsActive = true,
                            IsDeleted = false,
                            Name = "",
                            Password = "04f96727bb95e8cd75455822a7472e99a3fa14ce8098ffc5ce4a73ef07dde3fe",
                            ProfileImageUrl = "",
                            SurName = "",
                            UserGuid = new Guid("83b50aa5-1ca5-4f2d-9a69-c0d314c5f7f7"),
                            UserName = "demo"
                        },
                        new
                        {
                            Id = 6,
                            CreatedUserGuid = new Guid("00000000-0000-0000-0000-000000000000"),
                            IsActive = true,
                            IsDeleted = false,
                            Name = "",
                            Password = "04f96727bb95e8cd75455822a7472e99a3fa14ce8098ffc5ce4a73ef07dde3fe",
                            ProfileImageUrl = "",
                            SurName = "",
                            UserGuid = new Guid("207c1886-bb33-4710-8479-232891b0a7c5"),
                            UserName = "demo2"
                        });
                });

            modelBuilder.Entity("RoleUser", b =>
                {
                    b.HasOne("Yepz.TechJob.Domain.Entities.Role", null)
                        .WithMany()
                        .HasForeignKey("RolesId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Yepz.TechJob.Domain.Entities.User", null)
                        .WithMany()
                        .HasForeignKey("UsersId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Yepz.TechJob.Domain.Entities.Book", b =>
                {
                    b.HasOne("Yepz.TechJob.Domain.Entities.Author", "Author")
                        .WithMany("Books")
                        .HasForeignKey("AuthorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Author");
                });

            modelBuilder.Entity("Yepz.TechJob.Domain.Entities.Author", b =>
                {
                    b.Navigation("Books");
                });
#pragma warning restore 612, 618
        }
    }
}

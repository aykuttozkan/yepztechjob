﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Yepz.TechJob.Persistance.Migrations
{
    /// <inheritdoc />
    public partial class AddGeneratedGuidForUser_Book_Role_Author : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorGuid", "CreatedUserGuid" },
                values: new object[] { new Guid("e5d3eedd-a1c3-46c5-a9ce-19370197c9be"), new Guid("9dd43fcc-3810-4935-b22a-42115edc16cc") });

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorGuid", "CreatedUserGuid" },
                values: new object[] { new Guid("6200de17-bcc5-408c-9133-8f61c20f40bd"), new Guid("83b50aa5-1ca5-4f2d-9a69-c0d314c5f7f7") });

            migrationBuilder.UpdateData(
                table: "Role",
                keyColumn: "Id",
                keyValue: 3,
                column: "RoleGuid",
                value: new Guid("240340eb-dadb-470b-93f8-39d8755940c3"));

            migrationBuilder.UpdateData(
                table: "Role",
                keyColumn: "Id",
                keyValue: 5,
                column: "RoleGuid",
                value: new Guid("00d371f5-81cc-4c38-b949-bfc324c90327"));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                column: "UserGuid",
                value: new Guid("9dd43fcc-3810-4935-b22a-42115edc16cc"));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                column: "UserGuid",
                value: new Guid("83b50aa5-1ca5-4f2d-9a69-c0d314c5f7f7"));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                column: "UserGuid",
                value: new Guid("207c1886-bb33-4710-8479-232891b0a7c5"));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorGuid", "CreatedUserGuid" },
                values: new object[] { new Guid("8fb2e54b-3966-4495-ba41-511098e3e6c2"), new Guid("c1b802ad-f9ae-4aab-922a-2c4f3774108c") });

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorGuid", "CreatedUserGuid" },
                values: new object[] { new Guid("50562e4f-a8dd-4023-ba15-21fc079353b9"), new Guid("05e46352-23e2-49c5-8e3e-cda2afa0c690") });

            migrationBuilder.UpdateData(
                table: "Role",
                keyColumn: "Id",
                keyValue: 3,
                column: "RoleGuid",
                value: new Guid("a7ff0932-f0bb-4e0f-aa96-44e48eb30fa8"));

            migrationBuilder.UpdateData(
                table: "Role",
                keyColumn: "Id",
                keyValue: 5,
                column: "RoleGuid",
                value: new Guid("21337801-b7bd-4b5e-a7d8-7912c69a5e30"));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                column: "UserGuid",
                value: new Guid("c1b802ad-f9ae-4aab-922a-2c4f3774108c"));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                column: "UserGuid",
                value: new Guid("05e46352-23e2-49c5-8e3e-cda2afa0c690"));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                column: "UserGuid",
                value: new Guid("99204d74-4061-4cb8-a1ae-baf21989f2df"));
        }
    }
}

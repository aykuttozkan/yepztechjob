﻿using Microsoft.EntityFrameworkCore;
using Yepz.TechJob.Application.Interfaces;
using Yepz.TechJob.Domain.Entities;

namespace Yepz.TechJob.Persistance.Repositories;

public class BookWriteRepository : WriteRepository<Book>, IBookWriteRepository
{
    public BookWriteRepository(DbContext dbContext) : base(dbContext)
    {
    }
}


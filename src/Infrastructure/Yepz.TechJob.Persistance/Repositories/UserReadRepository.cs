﻿using Microsoft.EntityFrameworkCore;
using Yepz.TechJob.Application.Interfaces;
using Yepz.TechJob.Domain.Entities;

namespace Yepz.TechJob.Persistance.Repositories;

public class UserReadRepository : ReadRepository<User>, IUserReadRepository
{
    public UserReadRepository(DbContext dbContext) : base(dbContext)
    {
    }
}


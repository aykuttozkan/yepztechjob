﻿using Microsoft.EntityFrameworkCore;
using Yepz.TechJob.Application.Interfaces;
using Yepz.TechJob.Domain.Entities;

namespace Yepz.TechJob.Persistance.Repositories;

public class BookReadRepository : ReadRepository<Book>, IBookReadRepository
{
    public BookReadRepository(DbContext dbContext) : base(dbContext)
    {
    }
}


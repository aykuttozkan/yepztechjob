﻿using Microsoft.EntityFrameworkCore;
using Yepz.TechJob.Application.Interfaces;
using Yepz.TechJob.Domain.Common;

namespace Yepz.TechJob.Persistance.Repositories;

public class WriteRepository<T> : IWriteRepository<T> where T : BaseEntity
{
    private readonly DbContext _dbContext;

    public WriteRepository(DbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<T> AddAsync(T entity)
    {
        await _dbContext.AddAsync(entity);

        return entity;
    }

    public void Delete(T entity)
    {
        entity.IsDeleted = true;

        _dbContext.Entry(entity).State = EntityState.Modified;
    }

    public async Task DeleteByIdAsync(Guid id)
    {
        var entry = await _dbContext.Set<T>().FirstOrDefaultAsync(x => x.Id.Equals(id)) ?? throw new NullReferenceException(string.Format("{0} degeri null olamaz", nameof(T)));
        Delete(entry);
    }

    public Task<int> SaveChanges()
        => SaveChangesBase(null);

    public Task<int> SaveChanges(CancellationToken cancellationToken)
        => SaveChangesBase(cancellationToken);

    private async Task<int> SaveChangesBase(CancellationToken? cancellationToken)
    {
        if (cancellationToken == null)
            return await _dbContext.SaveChangesAsync();

        return await _dbContext.SaveChangesAsync(cancellationToken.Value);
    }
}


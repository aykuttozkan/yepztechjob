﻿using Microsoft.EntityFrameworkCore;
using Yepz.TechJob.Application.Interfaces;
using Yepz.TechJob.Domain.Entities;

namespace Yepz.TechJob.Persistance.Repositories;

public class AuthorReadRepository : ReadRepository<Author>, IAuthorReadRepository
{
    public AuthorReadRepository(DbContext dbContext) : base(dbContext)
    {
    }
}


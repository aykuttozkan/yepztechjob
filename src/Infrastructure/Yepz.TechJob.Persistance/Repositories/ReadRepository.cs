﻿using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Yepz.TechJob.Application.Interfaces;
using Yepz.TechJob.Domain.Common;

namespace Yepz.TechJob.Persistance.Repositories;

public class ReadRepository<T> : IReadRepository<T> where T : BaseEntity
{
    private readonly DbContext _dbContext;

    public ReadRepository(DbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<T> GetByIdAsync(Guid id)
        => await GetByIdAsync(id, false);

    public IQueryable<T> GetAllQueryable()
        => _dbContext.Set<T>();

    public IQueryable<T> GetAllQueryable(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includes)
    {
        var query = _dbContext.Set<T>().Where(predicate);

        foreach (var include in includes)
        {
            query = query.Include(include);
        }

        return query;
    }

    public async Task<T> GetByIdAsync(Guid id, bool isAsNoTracking)
    {
        if (isAsNoTracking)
            return (await _dbContext.Set<T>().Where(p => p.Id.Equals(id)).AsNoTracking().FirstOrDefaultAsync())!;

        return (await _dbContext.Set<T>().Where(p => p.Id.Equals(id)).FirstOrDefaultAsync())!;
    }
}

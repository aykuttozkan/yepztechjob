﻿using System;
namespace Yepz.TechJob.Commons.Models;

public class ResultModel<T> where T : class
{
    public T Result { get; set; }

    public bool IsError { get; set; } = false;

    public string ErrorMessage { get; set; } = string.Empty;

    private Exception? _exception { get; set; }

    public ResultModel()
    {

    }

    public ResultModel(T result)
    {
        Result = result;
    }

    private void ExceptionBase(Exception? exception, string errorMessage = "", bool isCustomErrorMessageEnabled = false)
    {
        if (exception == null)
            return;

        if (!string.IsNullOrWhiteSpace(errorMessage))
            ErrorMessage = errorMessage;
        else
        {
            if (!isCustomErrorMessageEnabled)
                ErrorMessage = exception.Message;
        }

        IsError = true;
        _exception = exception;
    }

    public void Exception(Exception exception) =>
        ExceptionBase(exception, string.Empty, false);

    public void Exception(Exception exception, string errorMessage) =>
        ExceptionBase(exception, errorMessage, true);
}

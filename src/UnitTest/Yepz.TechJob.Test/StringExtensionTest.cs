﻿using Yepz.TechJob.Commons.Extensions;

namespace Yepz.TechJob.Test;

public class StringExtensionTest
{
    [Fact]
    public void String_To_Base64()
    {
        string inputString = "merhaba";
        string outputString = inputString.ToBase64Encode();

        Assert.Equal("bWVyaGFiYQ==", outputString);
    }

    [Fact]
    public void String_To_Md5()
    {
        string inputString = "merhaba";
        string outputString = inputString.ComputeHashInLowerCase(HashingClassAlgorithms.MD5);

        Assert.Equal("c39436ee452e641cde2eb992ab397911", outputString);
    }

    [Fact]
    public void String_To_Sha256()
    {
        string inputString = "merhaba";
        string outputString = inputString.ComputeHashInLowerCase(HashingClassAlgorithms.SHA256);

        Assert.Equal("4c6bcdd55f3153e1939669ab1ec039e4059174dc25abdfcb2f58868849b4d61b", outputString);
    }
}
